from dataclasses import dataclass
from typing import List

@dataclass
class TextBox:
    x_a: int
    y_a: int
    x_b: int
    y_b: int
    text: str

    @staticmethod
    def create(x_a, y_a, x_b, y_b, text):
        return TextBox(x_a=x_a, y_a=y_a, x_b=x_b, y_b=y_b, text=text)


@dataclass
class Lot:
    no: int
    floor: str
    type: str
    size: int


@dataclass
class CoOwner:
    no: int
    title: str
    name: str
    address: str
    undiv: List[str]
    lots: List[Lot]

    def to_string(self):
        lines = []
        lines.append(f'No Copt: {self.no}')
        lines.append(f'Civ: {self.title}')
        lines.append(f'Nom: {self.name}')
        lines.append('Adresse:')
        lines.extend([ f'\t{x}' for x in self.address.splitlines()])
        lines.append('Indiv:')
        lines.extend([ f'\t{x}' for x in self.undiv])
        lines.append('Lot(s):')
        for lot in self.lots:
            lines.append(f'\tType: {lot.type}')
            lines.append(f'\t\tNo: {lot.no}')
            lines.append(f'\t\tTantièmes: {lot.size}')
            lines.append(f'\t\tEtage: {lot.floor}')
        lines.append('EndCopt')

        return '\n'.join(lines)

    def to_dict(self):
        l = [vars(lot) for lot in self.lots]
        d = vars(self)
        d['lots'] = l

        return d
