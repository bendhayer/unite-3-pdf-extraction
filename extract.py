from typing import List
from pathlib import Path
from datetime import date, datetime, timedelta
import argparse
import json

from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import (
    LAParams,
    LTChar,
    LTTextBoxHorizontal,
    LTTextLineHorizontal,
)

from data import TextBox, CoOwner, Lot

def log(msg):
    print(f'{datetime.now().isoformat()} - {msg}')


def parse_layout(layout) -> List[TextBox]:
    """Function to recursively parse the layout tree.

    Only horizontal text line (ie words) and char values will be filtered.

    Parameters
    ----------
    layout: pdfminer.layout.LTPage

    Returns
    -------
    boxes: List[TextBox]
    """
    boxes = []
    for lt_obj in layout:
        if isinstance(lt_obj, (LTTextLineHorizontal, LTChar)):
            text = lt_obj.get_text().replace('\n', '').strip()
            if text == '':
                print(f'\t\t\tIgnore empty: {list(lt_obj.bbox)}')
                continue
            boxes.append(TextBox.create(*lt_obj.bbox, text))
        elif isinstance(lt_obj, (LTTextBoxHorizontal)):
            children = parse_layout( lt_obj)
            if children is None:
                continue
            boxes.extend(children)
    if len(boxes) == 0:
        return None
    return boxes


def extract_textbox_from_pdf(file) -> List[List[TextBox]]:
    log('\tExtracting info from pdf')
    with open(file, 'rb') as pdf_file:
        doc = PDFDocument(PDFParser(pdf_file))
        pdf_pages = PDFPage.create_pages(doc)
        rsrcmgr = PDFResourceManager()
        device = PDFPageAggregator(rsrcmgr, laparams=LAParams(char_margin=0.1))
        page_interpretor = PDFPageInterpreter(rsrcmgr, device)

        pages = []
        for i, page in enumerate(pdf_pages, 1):
            log(f'\t\tExtracting page {i}')
            page_interpretor.process_page(page)
            page_layout = device.get_result()
            text_boxes = parse_layout(page_layout)
            pages.append(text_boxes)

        return pages


def intersect(tb1: TextBox, tb2: TextBox, axe='col') -> bool:
    if axe == 'col':
        return ((tb1.x_a <= tb2.x_a and tb2.x_a <= tb1.x_b)
                or (tb1.x_a <= tb2.x_b and tb2.x_b <= tb1.x_b))
    if axe == 'row':
        return ((tb1.y_a <= tb2.y_a and tb2.y_a <= tb1.y_b)
                or (tb1.y_a <= tb2.y_b and tb2.y_b <= tb1.y_b))


def get_data_from_section(section_text_boxes: List[TextBox]) -> CoOwner:
    # number of lot(s)
    nb_lot_tb = next(filter(lambda x: x.text.endswith('lot(s) visés'), section_text_boxes))
    nb_lot = int(nb_lot_tb.text.split()[0])
    # get name
    name = next(filter(
        lambda x: (
            intersect(x, nb_lot_tb, axe='row')
            and x.text != 'Total copropriétaire'
            and x != nb_lot_tb
            and not x.text.isdigit()
        ),
        section_text_boxes
    )).text
    # get columns
    section_data = sorted(
        list(filter(
            lambda x: not intersect(x, nb_lot_tb, axe='row') and x != nb_lot_tb,
            section_text_boxes
        )),
        key=lambda x: x.x_a
    )
    columns = [[section_data[0]]]
    curr_col = 0
    for tb in section_data[1:]:
        if intersect(columns[curr_col][0], tb):
            columns[curr_col].append(tb)
        else:
            columns.append([tb])
            curr_col += 1

    # get address and undiv
    address_col = sorted(columns[2], key=lambda x: x.y_a, reverse=True)
    normal_space = (address_col[0].y_a - address_col[1].y_b) * 1.3
    address = [address_col[0]]
    i = 1
    while i < len(address_col):
        if address[-1].y_a - address_col[i].y_b > normal_space:
            break
        address.append(address_col[i])
        i += 1
    address = '\n'.join([x.text for x in address])

    undiv = []
    while i < len(address_col):
        undiv.append(address_col[i].text)
        i += 1

    # get lots
    delta_col = 0
    no_lot_col = sorted(columns[3], key=lambda x: x.y_a, reverse=True)
    if len(undiv) > 0:
        no_lot_col = no_lot_col[:-len(undiv)]
    type_col = sorted(columns[4], key=lambda x: x.y_a, reverse=True)
    floor_col = []
    if len(columns) == 7:
        delta_col = -1
    else:
        floor_col = sorted(columns[5], key=lambda x: x.y_a, reverse=True)
    type2_col = sorted(columns[6 + delta_col], key=lambda x: x.y_a, reverse=True)
    size_col = sorted(columns[7 + delta_col], key=lambda x: x.y_a, reverse=True)
    if not(
        len(no_lot_col) == len(type_col)
        and len(no_lot_col) == len(type2_col)
        and len(no_lot_col) == len(size_col)
        and len(no_lot_col) == nb_lot
    ):
        raise Exception('Inconsistent data for lot')
    lots = []
    delta_garage = 0
    for i in range(len(no_lot_col)):
        floor = ''
        if type2_col[i].text == 'GARAGE':
            type2_col[i].text = 'PARKING'
        if type2_col[i].text == 'PARKING' or type2_col[i].text == 'CAVE':
            delta_garage += 1
        else:
            floor = floor_col[i - delta_garage].text

        lot = Lot(
            no=int(no_lot_col[i].text),
            size=int(size_col[i].text),
            floor=floor,
            type=type2_col[i].text
        )
        lots.append(lot)

    co_owner = CoOwner(
        no=int(columns[0][0].text),
        title=columns[1][0].text,
        name=name,
        address=address,
        undiv=undiv,
        lots=lots
    )
    return co_owner


def get_data_from_page(page: List[TextBox]) -> List[CoOwner]:
    first = sorted([x.y_a for x in filter(lambda x: x.text == 'Tantièmes', page)], reverse=True)
    if len(first) == 0:
        return []

    y_section = [first[0], *[x.y_a for x in filter(lambda x: x.text == 'Total copropriétaire', page)]]
    y_section = sorted(y_section, reverse=True)

    co_owners = []
    for i in range(len(y_section) - 1):
        upper = y_section[i]
        lower = y_section[i + 1]
        section_text_boxes = list(filter(lambda x: x.y_a < upper and x.y_b > lower and x.text != 0, page))
        co_owner = get_data_from_section(section_text_boxes)
        co_owners.append(co_owner)

    return co_owners


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('pdf')
    parser.add_argument('output')
    parser.add_argument('--format')
    args = parser.parse_args()

    log(f'Starting the extraction of {args.pdf}')
    pages = extract_textbox_from_pdf(args.pdf)

    co_owners = []
    nb_pages = len(pages)
    log(f'Analizing info')
    for i, page in enumerate(pages, 1):
        log(f'\tAnalyzing data for page {i} / {nb_pages}')
        cowns = get_data_from_page(page)
        co_owners.extend(cowns)

    log(f'Writing results to {args.output}')
    if args.format == 'json':
        with open(args.output, 'w', encoding='utf-8') as json_file:
            json.dump([c.to_dict() for c in co_owners], json_file)
    else:
        with open(args.output, 'w', encoding='utf-8') as txt_file:
            for c in co_owners:
                txt_file.write(c.to_string())
                txt_file.write('\n\n')

    log('Done')
