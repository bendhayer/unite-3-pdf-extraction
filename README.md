# Extraction de données d'une feuille de présence de l'unité 3

Ce script a pour objectif de lire et extraire toutes informations relatives aux copropriétaires de
l'unité 3 depuis les fichiers 'feuille de présence'.

Pour l'utiliser, il faut :
- [Python 3.8](https://www.python.org/downloads/) (ou supérieur)
- [Poetry](https://python-poetry.org/docs/)

Ensuite il suffit d'executer les commandes suivantes:
```shell
poetry install
poetry run python extract.py 'path/to/data.pdf' output.txt
```

*Note:* Poetry n'est pas nécessaire. Il est possible de s'en passer en installant les dépendances
spécifiées dans le fichier ``pyproject.toml`` manuellement.
